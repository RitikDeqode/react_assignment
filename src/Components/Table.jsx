import React from 'react';
import { connect } from 'react-redux';
import { deleteUser} from '../Redux'

function Table(props) {
    const userinfo = props.user
    return (
        <div className='datarender'>
        {
          userinfo.length > 0 && <table>
            <tbody>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Date of birth</th>
              <th>Gender</th>
              <th>Profile Pic</th>
              <th>Level of education</th>
              <th>Password</th>
              <th></th>
              <th></th>
            </tr>
            {userinfo.map((user, i) =>{
              return(
                <tr key={user.email}>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.DOB}</td>
                  <td>{user.gender}</td>
                  <td><img src={user.profilePic} alt=''></img></td>
                  <td>{user.levelOfEdu}</td>
                  <td>{user.password}</td>
                  <td><button onClick={()=>props.onHandleEdit(i)}>EDIT</button></td>
                  <td><button onClick={()=>props.deleteUser(user.email)}>DELETE</button></td>
                </tr>
              )
            })}
            </tbody>
          </table>
        }
        </div>
    )
}
const mapStateToProps = state => {
  return {
      user : state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
      deleteUser: email => dispatch(deleteUser(email))
  }
}



export default connect(
  mapStateToProps,
  mapDispatchToProps)
  (Table);
  