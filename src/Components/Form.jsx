import React,{useState} from 'react';
import { connect } from 'react-redux'
import Input from './Input';
import Table from './Table';
import validate from '../Utilities/Validate';
import { addUser, editUser } from '../Redux'
const initialState ={
    inde:-1,
    name:'',
    email:'',
    DOB:'',
    gender:'',
    profilePic:'https://www.shutterstock.com/image-vector/picture-image-icon-vector-illustration-1379376566',
    levelOfEdu:'',
    password:'',
    confirmPassword:'',
  }
const initialErrorState ={
    isValid:true,
    nameError:'',
    emailError:'',
    DOBError:'',
    genderError:'',
    levelOfEduError:''
}
function Form(props) {
    const [inputValue, setInputValue] = useState(initialState);
    const [error, setError] = useState(initialErrorState)
    
    const onChangeHandler = (e) =>{
        setInputValue({...inputValue,[e.target.name]:e.target.value});
    }
    const onChangeImage =(e)=>{
        const reader =new FileReader();
        reader.onload = () =>{
          if( reader.readyState === 2){
            setInputValue({...inputValue,profilePic:reader.result})
          }
        }
        reader.readAsDataURL(e.target.files[0])
    }
    const onHandleSubmit = (e) =>{
        let error = validate(inputValue)
        setError(error)
        e.preventDefault();
        if (error.isValid){
            if(inputValue.inde !== -1){
                setInputValue(initialState);
                props.editUser(inputValue);
            }
            else{
                props.addUser(inputValue)
                setInputValue(initialState);
            }
        }
    }
    
    const onHandleEdit = (i) =>{
        const nee = props.user[i];
        setInputValue({...nee, inde:i});
    }
  
    return (
        <div className='container'>
            <div className='box'>
            <h2>React Assignments</h2>
            <form>
                <label className='col'>
                    Name : 
                    <Input  type='text' name='name' value={inputValue.name} onChangeHandler={onChangeHandler}/>
                </label><br/>
                {<span style={{color:'red'}}>{error.nameError}</span>}
                <label>
                    Email : 
                    <Input type='email' name='email' value={inputValue.email} onChangeHandler={onChangeHandler}/>
                </label><br/>
                {<span style={{color:'red'}}>{error.emailError}</span>}
                <label>
                    DOB : 
                    <Input type='date' name='DOB' value={inputValue.DOB} onChangeHandler={onChangeHandler}/>
                </label><br/>
                {<span style={{color:'red'}}>{error.DOBError}</span>}
                <label>
                    Gender : 
                    <Input type='radio' name='gender' value="MALE" onChangeHandler={onChangeHandler}/>MALE
                    <Input type='radio' name='gender' value="FEMALE" onChangeHandler={onChangeHandler}/>FEMALE
                </label><br/>
                {<span style={{color:'red'}}>{error.genderError}</span>}
                <label>
                    Profile Pic : 
                    <Input type='file' name='profilePic' value="" onChangeHandler={onChangeImage}/>
                </label><br/>
                <label>
                    Level of Education :
                    <select name='levelOfEdu' value={inputValue.levelOfEdu} onChange={(e) => onChangeHandler(e)}>
                        <option value='NONE'>NONE</option>
                        <option value='10th'>10th</option>
                        <option value='12th'>12th</option>
                        <option value='Diploma'>Diploma</option>
                        <option value='Graduation'>Graduation</option>
                        <option value='Post Graduation'>Post Graduation</option>
                    </select>
                </label><br/>
                {<span style={{color:'red'}}>{error.levelOfEduError}</span>}
                <label>
                    Password : 
                    <Input type='password' name='password' value={inputValue.password} onChangeHandler={onChangeHandler}></Input>
                </label><br/>
                <label>
                    Confirm Password : 
                    <Input type='password' name='confirmPassword' value={inputValue.confirmPassword} onChangeHandler={onChangeHandler}></Input>
                </label><br/>
                <label>
                <button onClick={(e) => onHandleSubmit(e)}>{inputValue.inde > -1 ?"EDIT":"SUBMIT"}</button>
                </label>
            </form>
            </div>
            <Table onHandleEdit={onHandleEdit}/>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        user : state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addUser : value => dispatch(addUser(value)),
        editUser : value => dispatch(editUser(value)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps)
    (Form);
    