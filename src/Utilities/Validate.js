export default function Validate (inputValue){
    if((inputValue.password !== '' && inputValue.confirmPassword !== '')
        && (inputValue.password === inputValue.confirmPassword)){
            if(inputValue.name === ""){
                return {isValid:false,nameError:'Invalid name'};
            }

            if(inputValue.email === ''){
                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                if(!pattern.test(inputValue.email)){
                    return {isValid:false,emailError:'Email is not valid'};
                }
            }

            if(inputValue.DOB === ""){
                return {isValid:false,DOBError:'Please select date of birth'};
            }

            if(inputValue.gender === ""){
                return {isValid:false,genderError:'Please select gender'};
            }
            if(inputValue.levelOfEdu === ""){
                return {isValid:false, levelOfEduError:'Please select level of education'};
            }
        }
        else{
            alert('PASWORD IS NOT MATCHING')
            return {isValid:false}
        }
        return {isValid:true};
}
