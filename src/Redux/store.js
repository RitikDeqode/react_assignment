import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import formReducer from './form/formReducer';

const store = createStore(formReducer, applyMiddleware(logger));

export default store;
