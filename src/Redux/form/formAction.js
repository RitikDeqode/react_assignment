import {ADD_USER, EDIT_USER, DELETE_USER} from './formTypes';

export const addUser = ( value ) =>{
    return {
        type : ADD_USER,
        payload : value
    }
}

export const editUser = ( value ) =>{
    return {
        type : EDIT_USER,
        payload : value
    }
}

export const deleteUser = (email) =>{
    return {
        type : DELETE_USER,
        payload : email
    }
}
