import {ADD_USER, EDIT_USER, DELETE_USER} from './formTypes';

const inititalState = {
    user : []
}

const formReducer = (state = inititalState, action) =>{
    switch(action.type){
        case ADD_USER:
            return {...state,
                user: [...state.user,action.payload]
            }
        case EDIT_USER:
            const newdata = state.user;
            newdata[action.payload.inde] = action.payload;
            return {...state,
                user : newdata
            }
        case DELETE_USER:
            const newlist = state.user.filter(use => use.email !== action.payload)
            return {...state,
            user:newlist
        }
        default:
            return state
    }
}

export default formReducer;
