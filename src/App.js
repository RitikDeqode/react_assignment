import './App.css';
import React from 'react';
import Form from './Components/Form';
import { Provider } from 'react-redux';
import store from './Redux/store'

function App() {
  
  return (
    <div>
      <Provider store={store}>
        <Form/>
      </Provider>
    </div>
  );
}

export default App;
